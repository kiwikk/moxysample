Вся актуальная инфа есть в https://github.com/moxy-community/Moxy \
Там же есть связка с даггером \
moxy + cicerone http://bytepace.com/ru/blog/moxy-mvp-cicerone \
как работает в целом (55минут) https://www.youtube.com/watch?v=KDxYkqymTqA

## Kotlin
@Inject
lateinit var presenterProvider: Provider<MainPresenter>

private val presenter by moxyPresenter { presenterProvider.get() }

## Java
@InjectPresenter
MainPresenter presenter;

@Inject
Provider<MainPresenter> presenterProvider;

@ProvidePresenter
MainPresenter providePresenter() {
    return presenterProvider.get();
}

Последний коммит был 10 мая, имеют чатик в тг, внедряют котлиновские фишки тип делегатов и корутин. \
Имеют сообщество, которое помогает им искать и исправлять баги \
Презентер хранится в мапе-синглтоне, оттуда достаётся и прикрепляется к нужному фрагменту. \
отвязывается в onSaveInstanceState() \
удаляется в onDestroy (когда активи покидает бэкстэк)