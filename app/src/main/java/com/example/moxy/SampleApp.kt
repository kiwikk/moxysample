package com.example.moxy

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import androidx.lifecycle.ViewModelStoreOwner

class SampleApp : Application(), ViewModelStoreOwner {
    private val appViewModelStore = ViewModelStore()

    override fun getViewModelStore(): ViewModelStore {
        return appViewModelStore
    }

    fun getVM() : SampleViewModel{
        return ViewModelProvider(this).get(SampleViewModel::class.java)
    }
}