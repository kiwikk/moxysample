package com.example.moxy.activity1

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.example.moxy.R
import com.example.moxy.SampleApp
import com.example.moxy.activity2.SecondActivity
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class MainActivity : MvpAppCompatActivity(), MainView {
    @InjectPresenter
    lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainPresenter.vm = (application as SampleApp).getVM()
    }

    override fun onResume() {
        super.onResume()
        val button = findViewById<Button>(R.id.buttonGo)
        val add = findViewById<Button>(R.id.buttonAddToActivity)

        add.setOnClickListener {
            addText()
        }
        button.setOnClickListener {
            startActivity(Intent(this, SecondActivity::class.java))
        }
    }

    fun addText() {
        val et = findViewById<EditText>(R.id.editText)
        mainPresenter.addText(et.text.toString())
        et.setText("")
    }
}