package com.example.moxy.activity1

import com.example.moxy.SampleViewModel
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class MainPresenter : MvpPresenter<MainView>() {
    lateinit var vm:SampleViewModel

    fun addText(text: String) {
        vm.text.add(text)
    }
}