package com.example.moxy.activity2

import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.example.moxy.R
import com.example.moxy.SampleApp
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class SecondActivity:MvpAppCompatActivity(), SecondView {
    @InjectPresenter
    lateinit var presenter: SecondPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity2_layout)
        presenter.vm = (application as SampleApp).getVM()
        val btn = findViewById<Button>(R.id.button)
        btn.setOnClickListener { presenter.addText() }
    }

    override fun add(text: String) {
        val layout = findViewById<LinearLayout>(R.id.linearLayout)
        val tv = TextView(this)
        tv.text = text
        layout.addView(tv)
    }
}