package com.example.moxy.activity2

import com.example.moxy.SampleViewModel
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class SecondPresenter : MvpPresenter<SecondView>() {
    var count = 0
    lateinit var vm: SampleViewModel

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.add("First")
        for(t in vm.text){
            viewState.add(t)
        }
    }

    fun addText() {
        viewState.add("Not first: ${++count}")
    }
}